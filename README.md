# Just Enough Prep

A FREE introductory course to get you and your computer ready for _any_ **Just Enough** course. Learn the basics of the Command Line Interface (CLI), installing developer tools, source control, and using a professional code editor on Mac, Linux, and Windows.

## Course Outline

* **What will we learn?**
  * What is Web Development?
  * What makes a _professional_...?
  * What tools will we use?
    * A Professional Code Editor
    * A Professional Web Browser
    * A Source Control System
    * A Command Line Interface
  * What techniques will we use?
    * Branching and Forking
    * Automated Testing
    * Pair Programming
* **What is Web Development?**
* **What makes a _professional_...?**
  * Tools.
  * Techniques.
  * Skill.
  * Experience.
* **What _tools_ do we need?**
  * A Professional Code Editor -- Atom
    * Basic Features of an Editor
    * Features of a _Professional_ Code Editor
    * Why use Atom?
    * Why use Cloud9 IDE?
    * Why not use something simpler?
  * A Professional Browser -- Chrome
    * Basic Features of a Web Browser
    * Features of a Professional Web Browser
    * Why use Chrome? Why not Firefox?
    * What is Cross-Browser Testing?
  * A Source Control System -- `git` and GitHub.com
    * Basic Features of a Source Control System
    * Why use `git`? Why use GitHub?
    * Why not use something simpler?
  * A Command Line Interface -- `zsh` + Prezto
    * Basic Features of a CLI
    * Features of a Professional CLI
    * Why use ZSH? Why not Bash?
    * Why use Prezto?
    * What's wrong with a GUI?
* **Time for an Install-fest!**
  * Step 0 -- Install and Configure Developer Tools
    * Mac:
      * Install XCode / Developer Tools
      * Install MacPorts
    * Win:
      * Install Babun (Cywin+)
      * Instsall `zsh`
    * Lin:
      * Wait for everyone else...
    * Install some packages...
      * What's a package?
      * Why do we need them?
      * What will we need to install?
    * Dotfiles.
  * Step 1 -- Create Accounts on:
    * LastPass: http://lastpass.com/signup
    * GitHub: http://github.com/signup
    * Cloud9: https://c9.io/signup -- Use GitHub!
  * Step 2 -- Install and Configure Atom
    * Install `atom-beautify`
  * Step 3 -- Install and Configure Chrome
    * Turn off caching.
    * ...?
  * Step 4 -- Install and Configure GitHub Desktop
    * Login with your GitHub Account (using LastPass)
  * Step 5 -- Install and Configure CLI Tools
    * Fork, install, and configure Prezto
